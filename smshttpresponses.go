package ncsmsgo

import (
	"log"
	"strconv"
)

// SmsVersionResponse version response
// swagger:response smsVersionResponse
type SmsVersionResponse struct {
	// in: body
	Body struct {
		// Version
		// required: true
		Version int `json:"version"`
	}
}

// SmsPushResponse push response
// swagger:response smsPushResponse
type SmsPushResponse struct {
	// in: body
	Body struct {
		// Status
		// required: true
		Status bool `json:"status"`

		// Message
		// required: true
		Message string `json:"msg"`
	}
}

// Return response status
func (spr *SmsPushResponse) GetStatus() bool {
	return spr.Body.Status
}

// Returns response message
func (spr *SmsPushResponse) GetMessage() string {
	return spr.Body.Message
}

type SmsPhoneListResponse struct {
	// in: body
	Body struct {
		// phoneList
		// required: true
		PhoneList []string `json:"phoneList"`
	}

	len int
	cursor int
}

// Returns next entry in phone list.
// If no more entry or empty, returns empty string
func (slr *SmsPhoneListResponse) GetNextEntry() string {
	if slr.cursor >= slr.len {
		return ""
	}

	res := slr.Body.PhoneList[slr.cursor]
	slr.cursor++
	return res
}

type SmsIDListResponse struct {
	// in: body
	Body struct {
		// smslist
		// required: true
		SmsList []string `json:"smslist"`
	}
}

type SmsMessage struct {
	Address string `json:"address"`
	Mailbox int `json:"mailbox"`
	Message string `json:"msg"`
	Type int `json:"type"`
	Date int64 `json:"omit"`// This attribute is only used on transformation
}

type SmsMessagesResponse struct {
	// in: body
	Body struct {
		// messages
		// required: true
		Messages map[string]SmsMessage `json:"messages"`
		LastID int64 `json:"last_id"`
	}

	messages []SmsMessage
	len int
	cursor int
}

func (smr *SmsMessagesResponse) transformResponse() {
	var err error
	for k, m := range smr.Body.Messages {
		m.Date, err = strconv.ParseInt(k, 10, 64)
		if err != nil {
			log.Printf("Invalid message date key format: '%s', ignoring. Error was: %s\n", k, err)
			continue
		}
		smr.messages = append(smr.messages, m)
	}

	smr.len = len(smr.messages)
	smr.cursor = 0
}

func (smr *SmsMessagesResponse) GetLastID() int64 {
	return smr.Body.LastID
}

// Returns next entry in phone list.
// If no more entry or empty, returns nil
func (smr *SmsMessagesResponse) GetNextMessage() *SmsMessage {
	if smr.cursor >= smr.len {
		return nil
	}

	res := &smr.messages[smr.cursor]
	smr.cursor++
	return res
}