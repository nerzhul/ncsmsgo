package ncsmsgo

type Sms struct {
	ID int `json:"_id"`
	Mailbox int `json:"mbox"`
	Type int `json:"type"`
	Date int64 `json:"date"`
	Body string `json:"body"`
	Address string `json:"address"`
	Read string `json:"read"`
	Seen string `json:"seen"`
}

type SmsPushQuery struct {
	SmsCount int  `json:"smsCount"`
	SmsData []Sms `json:"smsDatas"`
}