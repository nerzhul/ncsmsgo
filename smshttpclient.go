package ncsmsgo

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const (
	// APIv1
	rcallGetVersion             = "/index.php/apps/ocsms/get/apiversion?format=json"
	rcallGetAllSmsIds           = "/index.php/apps/ocsms/get/smsidlist?format=json"
	rcallGetLastMsgTimestamp    = "/index.php/apps/ocsms/get/lastmsgtime?format=json"
	rcallPushRoute              = "/index.php/apps/ocsms/push?format=json"

	// APIv2
	rcallV2GetPhoneList         = "/index.php/apps/ocsms/api/v2/phones/list?format=json";
	rcallV2GetMessages          = "/index.php/apps/ocsms/api/v2/messages/[START]/[LIMIT]?format=json";
	rcallV2GetMessagesPhone     = "/index.php/apps/ocsms/api/v2/messages/[PHONENUMBER]/[START]/[LIMIT]?format=json";
	rcallV2GetMessagesSendQueue = "/index.php/apps/ocsms/api/v2/messages/sendqueue?format=json";
)

type SmsHTTPClient struct {
	instanceURL string
	userAgent string
	userName string
	password string
	lastHTTPStatus int
	lastError string
	client *http.Client
}

func (s *SmsHTTPClient) Init(url string, appVersion string, userName string, password string, insecure bool) {
	s.instanceURL = url
	s.userAgent = "nextcloud-phonesync-go (" + appVersion + ")"
	s.userName = userName
	s.password = password

	tr := &http.Transport{}
	if insecure {
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}

	s.client = &http.Client{Transport: tr}
}

func (s *SmsHTTPClient) GetLastHTTPStatus() int {
	return s.lastHTTPStatus
}

func (s *SmsHTTPClient) GetLastMsgTimestamp() string {
	return rcallGetLastMsgTimestamp
}

func (s *SmsHTTPClient) GetLastError() string {
	return s.lastError
}

func (s *SmsHTTPClient) createHTTPRequest(verb string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(verb, url, body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", s.userAgent)
	req.SetBasicAuth(s.userName, s.password)
	return req, err
}

func (s *SmsHTTPClient) performHTTPRequest(verb string, url string, body io.Reader) *http.Response {
	s.lastHTTPStatus = 0
	req, err := s.createHTTPRequest(verb, url, body)
	if err != nil || req == nil {
		fmt.Println(err)
		s.lastError = err.Error()
		return nil
	}

	resp, err := s.client.Do(req)
	if err != nil {
		fmt.Println(err)
		s.lastError = err.Error()
		return nil
	}

	s.lastHTTPStatus = resp.StatusCode
	return resp
}

// DoVersionCall call server version
// This function return version id
// If negative or nil an error occurs
// -1 : reading error
// > 0 : server version
func (s *SmsHTTPClient) DoVersionCall() int {
	resp := s.performHTTPRequest("GET", s.instanceURL + rcallGetVersion, nil)
	if resp == nil {
		return -1
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return -1
	}

	vResp := &SmsVersionResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&vResp.Body); err != nil {
		s.lastError = err.Error()
		return 1
	}

	if vResp.Body.Version == 0 {
		log.Println("DoVersionCall: no version field in response, assume version 1")
		return 1
	}

	return vResp.Body.Version
}

// DoPushCall push SmsBuffer to server
// This function returns SmsPushResponse
// If nil an error occurs
// nil : reading error
// not nil : server response
func (s *SmsHTTPClient) DoPushCall(buffer *SmsBuffer) *SmsPushResponse {
	resp := s.performHTTPRequest("POST", s.instanceURL + rcallPushRoute,
		strings.NewReader(buffer.AsRawJSONString()))
	if resp == nil {
		return nil
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil
	}

	pResp := &SmsPushResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&pResp.Body); err != nil {
		s.lastError = err.Error()
		return nil
	}

	return pResp
}

// DoGetPhoneList retrieve phonelist from server
// This function returns SmsPhoneListResponse
func (s *SmsHTTPClient) DoGetPhoneList() *SmsPhoneListResponse {
	resp := s.performHTTPRequest("GET", s.instanceURL + rcallV2GetPhoneList, nil)
	if resp == nil {
		return nil
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil
	}

	vResp := &SmsPhoneListResponse{
		cursor: 0,
		len: 0,
	}
	if err := json.NewDecoder(resp.Body).Decode(&vResp.Body); err != nil {
		s.lastError = err.Error()
		return nil
	}

	vResp.len = len(vResp.Body.PhoneList)

	return vResp
}

// DoGetSmsIDList retrieve SMS id list from server
// This function returns SmsIDListResponse
func (s *SmsHTTPClient) DoGetSmsIDList() *SmsIDListResponse {
	resp := s.performHTTPRequest("GET", s.instanceURL + rcallGetAllSmsIds, nil)
	if resp == nil {
		return nil
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil
	}

	vResp := &SmsIDListResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&vResp.Body); err != nil {
		s.lastError = err.Error()
		return nil
	}

	return vResp
}

// DoGetMessagesCall retrieve SMS list from server
// This function returns SmsMessagesResponse
func (s *SmsHTTPClient) DoGetMessagesCall(start int64, limit int) *SmsMessagesResponse {
	fixedEndpoint := strings.Replace(
		strings.Replace(rcallV2GetMessages, "[START]", strconv.FormatInt(start, 10), -1),
		"[LIMIT]", strconv.Itoa(limit), -1)

	resp := s.performHTTPRequest("GET", s.instanceURL + fixedEndpoint, nil)
	if resp == nil {
		log.Println("DoGetMessagesCall: unable to performHTTPRequest")
		return nil
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Printf("DoGetMessagesCall: status code %d != 200\n", resp.StatusCode)
		return nil
	}

	smsResp := &SmsMessagesResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&smsResp.Body); err != nil {
		s.lastError = err.Error()
		log.Printf("DoGetMessagesCall: Unable to decode JSON response from server. Error was: %s\n", err)
		return nil
	}

	// Reformat response properly for java bindings
	smsResp.transformResponse()

	return smsResp
}