package ncsmsgo

import (
	"encoding/json"
)

type SmsBuffer struct {
	lastMessageDate int64
	spq SmsPushQuery
}

func (b *SmsBuffer) Clear() {
	b.lastMessageDate = 0
	b.spq = SmsPushQuery{}
}

func (b *SmsBuffer) Empty() bool {
	return len(b.spq.SmsData) == 0
}

func (b *SmsBuffer) AsRawJSONString() string {
	bspq, err := json.Marshal(b.spq)
	if err != nil {
		println("Failed to generate JSON String from SmsPushQuery")
		return ""
	}
	return string(bspq)
}

func (b *SmsBuffer) Push(ID int, mailboxID int, t int, date int64, address string, body string, read string,
	seen string) {
	b.spq.SmsData = append(b.spq.SmsData, Sms{
		ID: ID,
		Mailbox: mailboxID,
		Type: t,
		Date: date,
		Address: address,
		Body: body,
		Read: read,
		Seen: seen,
	})

	b.spq.SmsCount++

	if date > b.lastMessageDate {
		b.lastMessageDate = date
	}
}

func (b *SmsBuffer) GetLastMessageDate() int64 {
	return b.lastMessageDate
}